﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad2
{
    class Program
    {
        int infinite = int.MaxValue;

        static void Main(string[] args)
        {

            Program pro = new Program();
            pro.readData();

        }

        private void readData()
        {
            int testCount = Convert.ToInt32(Console.ReadLine());

            for (int t = 0; t < testCount; t++)
            {
                string[] nodeEdgeCount = Console.ReadLine().Split(' ');

                int nodeCount = Convert.ToInt32(nodeEdgeCount[0]);
                int edgeCount = Convert.ToInt32(nodeEdgeCount[1]);

                int[,] w = new int[nodeCount, nodeCount];

                for (int i = 0; i < nodeCount; i++)
                {
                    for (int j = 0; j < nodeCount; j++)
                    {
                        w[i, j] = infinite;
                    }
                }

                for (int i = 0; i < edgeCount; i++)
                {
                    string[] nodeWeight = Console.ReadLine().Split(' ');

                    int nodeX = Convert.ToInt32(nodeWeight[0]);
                    int nodeY = Convert.ToInt32(nodeWeight[1]);
                    int weight = Convert.ToInt32(nodeWeight[2]);

                    w[nodeX - 1, nodeY - 1] = weight;
                    w[nodeY - 1, nodeX - 1] = weight;
                }

                Prim(w);

            }

        }

        private void Prim(int[,] w)
        {
            int N = w.GetLength(0);
            int[] near = new int[N];
            int[] dist = new int[N];
            int s = 0;

            HashSet<int> V = new HashSet<int>();
            HashSet<int> Vt = new HashSet<int>();

            for(int i = 0; i<N; i++)
            {
                V.Add(i);
            }

            near[s] = 0;

            Vt.Add(s);
            V.Remove(s);

            foreach (int v in V)
            {
                near[v] = s;
                dist[v] = w[s, v];
            }

            while (Vt.Count < N)
            {
                HashSet<int> VSubVt = new HashSet<int>(V);
                VSubVt.ExceptWith(Vt);

                int u = 0;
                int min = infinite;

                foreach (int v in VSubVt)
                {
                    if (dist[v] <= min)
                    {
                        min = dist[v];
                        u = v;
                    }
                }

                Vt.Add(u);

                if (dist[u] == infinite)
                {
                    Console.WriteLine("brak");
                    return;
                }

                foreach (int v in VSubVt)
                {
                    if (w[u, v] < dist[v])
                    {
                        dist[v] = w[u, v];
                        near[v] = u;
                    }
                }
            }

            Console.WriteLine(dist.Sum());
        }

    }
}
